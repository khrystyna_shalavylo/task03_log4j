package com.epam.controller;


import com.epam.model.Fibonacci;
import com.epam.view.ConsoleView;
import com.epam.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {

    private final static Logger logger = LogManager.getLogger(Controller.class);

    private static View view = new ConsoleView();

    public static void main(String[] args) {
        Fibonacci fibonacci=new Fibonacci();
        view.showFibonacci();
        logger.warn("Warn");
        logger.error("Error");
        logger.fatal("Fatal");
        try
        {
            fibonacci.summingOddNumbers();
        }
        catch(Exception e)
        {
            logger.error("Unknown error", e);
        }
    }
}