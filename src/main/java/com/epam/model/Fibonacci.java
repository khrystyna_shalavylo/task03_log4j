package com.epam.model;

public class Fibonacci {

    private static final int SIZE = 10;
    private static int[] fibonacci;

    public Fibonacci(){
        fibonacci= new int[SIZE];
    }
    public static String getFibonacci() {
        String fib="";
        int fibonPrevious = 0;
        int fibonCurrent = 1;
        fibonacci[0] = fibonPrevious;
        fibonacci[1] = fibonCurrent;
        for (int i = 0; i < SIZE; i++) {
            int copy = fibonCurrent;
            fibonacci[i] += fibonPrevious + fibonCurrent;
            fibonCurrent += fibonPrevious;
            fibonPrevious = copy;
        }
        for (int i=0;i<SIZE;i++){
            fib=fibonacci[i]+" ";
        }

        return fib;
    }

    public int summingOddNumbers() throws Exception{
        int result = 0;
        for (int i = 0; i < fibonacci.length; i ++) {
            if (fibonacci[i]%2!=0) {
                result += i;
            }
        }
        if(result>1000){
            throw new  Exception ();
        }
        return result;
    }
}

