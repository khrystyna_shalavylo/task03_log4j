package com.epam.view;

import com.epam.model.Fibonacci;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView implements View {
    private Logger logger= LogManager.getLogger(ConsoleView.class);
    public void showFibonacci() {
        logger.info("Fibonacci row : " + Fibonacci.getFibonacci());
    }

}
